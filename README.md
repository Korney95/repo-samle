# Repo Samle

## This is client-server app

# First step (server)
1) `create docker image from docker file with openssh server and webserver (nginx) base image Centos8`
2) `run nginx and sshd in ONE container on custom ports` **ssh**: 2222 **webserver**:8080 
3) `add the ability to add a connection to the container via ssh use public ssh rsa key` FOR EXAMPLE: 
```sh
ssh -p 2222 root@localhost
```
4) `create a pull reqest to this repo` Good Luck! :)

#############################

Add you ssh public key to ssh_keys/autorized_key

Build container:

docker build -t image-name .

Start:

docker run -d -p 2222:22 -p 8080:80 container-name image-name
