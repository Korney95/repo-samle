FROM centos:centos8

RUN cd /etc/yum.repos.d/ && \
    sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-* && \
    sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-* && \
    yum install -y epel-release && \
    yum update -y && \ 
    yum upgrade -y && \
    yum install -y nginx openssh-server sudo

RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N '' && \
    ssh-keygen -t dsa  -f /etc/ssh/ssh_host_dsa_key -N '' && \
    ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N '' && \
    ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N '' && \
    chmod 600 /etc/ssh/*

ADD ssh_keys/authorized_keys /tmp/authorized_keys

RUN mkdir -p /var/run/sshd && \
	mkdir -p /root/.ssh && \
	cat /tmp/authorized_keys >> /root/.ssh/authorized_keys && \
	rm /tmp/authorized_keys

ADD nginx/default           /etc/nginx/sites-available/default
ADD nginx/index.html        /usr/share/nginx/html/index.html

EXPOSE 22 80

CMD bash -c /usr/sbin/nginx -g ; /usr/sbin/sshd -D
